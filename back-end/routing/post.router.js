"use strict";

const express = require('express');
const route = express.Router();

const strings = require('../helpers/string_utils');
const upload = require('../helpers/uploadConfig');

const Post = require('../models/Post');

const bodyParser = require('body-parser');

route.use(bodyParser.urlencoded({
    extended: false
}));

route.post('/add-post.html', upload.single('imagePost'),(req, res)=>{
    // title, contentSmall, contentLarge, image, category
    const { title, contentSmall, contentLarge, category} = req.body;
    const { imagePost } = req.file ? req.file.filename : 'default.png';
    Post.addPost(title, contentSmall, contentLarge, image, category)
        .then(() =>{
             res.redirect('admin');
        })
});

route.delete('/delete-post/:idPost.html', (req, res)=>{
    const { idPost } = req.params;
    Post.removePost(idPost)
        .then(()=>{
             res.redirect('/admin');
        }).catch(err =>{
            console.log(err.message);
        });
});

route.post('/get-list-post', (req, res)=>{
    const { startTime, endTime } = req.body;
    Post.getListToFrom(startTime, endTime)
        .then(list =>{
            // TODO
        }).catch(err =>{
            console.log(err.message);
        });
});

route.get('/detail-post/:idPost.html', (req, res)=>{
    const  { idPost }  = req.params;
    Post.infoPost(idPost)
        .then(infoPost => {
            // TODO
        }).catch(err => console.log(err.message));
});

module.exports = route;