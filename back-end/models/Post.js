const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const fs = require('fs');



const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {type: String, required: true, trim: true},
    contentSmall: {type: String, required: true},
    category: {type: String, required: true},
    contentLarge: {type: String, required: true},
    image: {type: String},
    statusCurrent: {type: Number, default: 1}, //1.active 0.block
    createAt: {type: Date, default: Date.now()},
    updateAt: {type: Date, default: Date.now()}
});

const PostMongo = mongoose.model('Post', PostSchema);

class Post extends PostMongo{
    static async addPost(title, contentSmall, contentLarge, image, category){
        const post = new Post(title, contentSmall, contentLarge, image, category);
        const postNew = await post.save();
        if(!postNew) throw new error('add_post_fail');
        return postNew;
    }

    static async removePost(idPost){
        const postResult = await Post.findByIdAndUpdate(idPost, {
            statusCurrent: 0,
            updateAt: Date.now()
        })
        if(!postResult) throw new error('cannot_find_post_by_id');
        if(postResult.image === 'default.png') return;
        /**
         * Nếu hình ảnh ban đầu là mặc định của máy thì không thực hiện tiếp
         */
        
         const path = './public/image/'+postResult.image;
           /*
            Tìm đường dẫn vào xóa
           */     
          fs.unlink(path, err =>{
              if(err) console.log(err);
          });

          return postResult;
    }

    static async getListToFrom(startTime, endTime){
        var condition;
        if(startTime === undefined || endTime === undefined){
            condition = {
                statusCurrent: 1
            }
        }else{
            condition = {
                statusCurrent: 1,
                createAt: {$gte: startTime, $lte: endTime}
            };
        }
        const listPost = await Post.find(condition);
        if(!listPost) throw new error('cannot_get_list');
        return listPost;
    }

    static async infoPost(idPost){
        const postResult = await Post.findById(id);
        if(!postResult) throw new error('cannot_find_post_by_id');
        return postResult;
    }
}

module.exports = Post;