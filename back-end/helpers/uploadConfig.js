const multer = require('multer');
const strings = require('./string_utils');

const storage = multer.diskStorage({
    destination: (req, file, cb) => cb(null, './public/images/post'),
    filename: (req, file, cb) => cb(null, file.originalname + strings.randomAffNumber(10) )
});

function fileFilter(req, file, cb) {
    const { mimetype } = file;
    if (mimetype === 'image/png' || mimetype === 'image/jpeg') {
        return cb(null, true);
    }
    cb(new Error('File khong dung dinh dang!'));
}


const upload = multer({ storage, fileFilter });

module.exports = upload;