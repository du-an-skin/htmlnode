const express = require('express');
const mongoose = require('mongoose');

const app = express();

// import route
const routePost = require('./routing/post.router');

// app config
app.set('views', './views');
app.set('view engine', 'ejs');
app.use(express.static('./public'));

app.use('/post', routePost);

app.use('*', (req, res)=>{
    res.render('index');
})

const uri = 'mongodb://localhost/vaniphi';
mongoose.connect(uri);
mongoose.connection.once('open', ()=>{
    app.listen(3000, ()=>{
        console.log('Server started at port 3000');
    });
});

